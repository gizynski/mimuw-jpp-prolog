/* ------ recursion, exercise 2 -------- */

taller(bob, mike).
taller(mike, jim).
taller(jim, george).

% tutaj kolejnosc klauzul nie ma znaczenia, jesli poprosimy o kolejna ; to i tak bedzie ERROR: Out of local stack
taller(X, Y) :-
	taller(X, Z),
	taller(Z, Y).

/* ------ recursion, exercise 3 -------- */

can_get(town1, town2).
can_get(town2, town3).
can_get(town3, town4).
can_get(town4, town5).
can_get(town5, town6).

can_get(X,Y) :-
	can_get(X, Z),
	can_get(Z, Y).

% tutaj dla can_get(town3, town1). dostajemy tez ERROR: Out of local stack.
% co musze zrobic zeby takie bledy sie nie pojawialy, a zamiast tego byla zwykla odpowiedz ze nie, nie mozna?
% a moze ja zle zapisuje ta rekurencje?

/* ------ list constructions, exercise 1 ------- */

% ?- delete_all([a,b,a,c,a,d],a,Result).
% Result = [b,c,d]

delete_all([], _, []).

delete_all([H|T], H, TailWithDeletedXs) :-
	delete_all(T, H, TailWithDeletedXs).

delete_all([H|T], X, [H|TailWithDeletedXs]) :-
	delete_all(T, X, TailWithDeletedXs).

% wszystko elegancko dziala

/* ------ list constructions, exercise 1 ------- */

% ?- replace_all([a,b,a,c,a,d],a,mike,Result).
% Result = [mike,b,mike,c,mike,d]

replace_all([], _, _, []).

replace_all([H|T], H, WithWhatReplace, [WithWhatReplace|Rest]) :-
	replace_all(T, H, WithWhatReplace, Rest).

replace_all([H|T], WhatToReplace, WithWhatReplace, [H|Rest]) :-
	replace_all(T, WhatToReplace, WithWhatReplace, Rest).
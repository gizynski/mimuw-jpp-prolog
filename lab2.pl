% ---------- cwiczenia na akumulatory ---------

% a) suma(L, S) wtw, gdy S = suma elementów listy L
suma(L, S) :- 
	sumaA(L, 0, S).
sumaA([], Acc, S) :- 
	S = Acc.
sumaA([H|T], Acc, S) :-
	NewAcc is Acc + H,
	sumaA(T, NewAcc, S).
/* dzialanie
?- suma([12,3,3,5,543,54,3], S).
S = 623.

?- suma([], S).
S = 0.

?- suma([2], S).
S = 2.
*/

% b) dlugosc(L, K) wtw, gdy K = liczba elementów listy L (length/2)
dlugosc(L, D) :- 
	dlugoscA(L, 0, D).
dlugoscA([], K, D) :-
	D = K.
dlugoscA([_|T], K, D) :-
	NewK is K + 1,
	dlugoscA(T, NewK, D).
/* dziala to tak:
?- dlugosc([2], D).
D = 1.

?- dlugosc([12,3,3,5,543,54,3], D).
D = 7.

?- dlugosc([], D).
D = 0.
*/

% c) min(L, M) wtw, gdy M jest minimalnym elementem L (L = lista np. liczb całkowitych)
min([H|T], M) :-
	minA(T, H, M).
minA([], CurrentMin, M) :-
	M = CurrentMin.
minA([H|T], CurrentMin, M) :-
	H < CurrentMin,
	minA(T, H, M).
minA([H|T], CurrentMin, M) :-
	H >= CurrentMin,
	minA(T, CurrentMin, M).
/* dzialanie:
?- min([12,3,-3,5,543,54,3], M).
M = -3 .

?- min([4],M).
M = 4.

?- min([],M).
false.
*/

% d) odwroc(L, R) wtw, gdy R jest odwróconą listą L (np. odwroc([1,2,3,4], [4,3,2,1]) - sukces)
odwroc(L, R) :-
	odwrocA(L, [], R).
odwrocA([], A, R) :-
	R = A.
odwrocA([H|T], A, R) :-
	odwrocA(T, [H|A], R).
/* dzialanie:
?- odwroc([12,3,-3,5,543,54,3], O).
O = [3, 54, 543, 5, -3, 3, 12].

?- odwroc([],O).
O = [].

?- odwroc([4],O).
O = [4].
*/

% e) palindrom(Slowo) wtw, gdy (lista) Slowo jest palindromem (np. palindrom([k,a,j,a,k]), palindrom([1,b,2,b,1]) - sukcesy)
palindrom(L) :- 
	palindromA(L, [], R),
	L == R.
palindromA([], A, R) :-
	R = A.
palindromA([H|T], A, R) :-
	palindromA(T, [H|A], R).
/* dzialanie:
?- palindrom([1,2,3]).
false.

?- palindrom([1,2,1]).
true.

?- palindrom([]).
true.

?- palindrom([k,a,j,a,k]).
true.
*/

% f) slowo(Slowo) == Slowo= a^nb^n (Uwaga: bez arytmetyki!) dwa warianty: (*) n > 0 (**) n >= 0 (np. slowo([a,a,b,b]) - sukces)
slowo([a|T]) :-
	slowoA(T, [a], a).
slowoA([], [], b).
slowoA([a|T], A, a) :-
	slowoA(T, [a|A], a).
slowoA([b|T], [a|AT], _) :-
	slowoA(T, AT, b).
/*
?- slowo([a,a,a,b,b,b]).
true ;
false.

?- slowo([a,b,a,b]).
false.

?- slowo([b,b,a,a]).
false.

?- slowo([a,a,b,b,b]).
false.

?- slowo([a,a,a,b,b]).
false.

?- slowo([a,b]).
true.

?- slowo([a]).
false.

?- slowo([b]).
false.

?- slowo([]).
false.
*/

% drugi wariant to po prostu rozszerzenie juz zdefiniowanego o akceptowanie przypadku gdy slowo jest puste
slowo0([]).
slowo0([a|T]) :- 
	slowoA(T, [a], a).

% g) slowo(Zdanie, Reszta) == Zdanie = Slowo * Reszta, Slowo - jw. (np. slowo([a,a,b,b,c,d], [c,d]) - sukces)
reszta([a|T], Reszta) :-
	resztaA(T, [a], a, Reszta).
resztaA([], [], _, []).
resztaA([H|T1], [], _, [H|T2]) :-
	resztaA(T1, [], H, T2).
resztaA([a|T], A, a, Reszta) :-
	resztaA(T, [a|A], a, Reszta).
resztaA([b|T], [a|AT], _, Reszta) :-
	resztaA(T, AT, b, Reszta).
/*
?- reszta([a,a,a,b,b,b,c,d], [c,d]).
true ;
false.

?- reszta([a,a,a,b,b,b,c,d], [b,c,d]).
false.

?- reszta([a,a,a,b,b,b,c,d], [c,c,d]).
false.

?- reszta([a,b], []).
true.
*/

% h) flagaPolska(Lista, Flaga) wtw, gdy Flaga jest posortowaną listą Lista, złożoną ze stałych b,c (np. flagaPolska([b,c,b,c], [b,b,c,c]) - sukces)
% uwaga - zakladam ze zarowno c, jak i b musza sie pojawic po jednym razie przynajmniej (inaczej flaga jest niepelna)
% uwaga2 - troche zle zrozumialem polecenie i zrobilem sprawdzenie czy dana lista jest flaga polska, poprawiam sie przy fladze holenderskiej
flagaPolska([b|T]) :-
	flagaPolskaA(T, b).
flagaPolskaA([], c).
flagaPolskaA([b|T], b) :-
	flagaPolskaA(T, b).
flagaPolskaA([c|T], _) :-
	flagaPolskaA(T, c).
/*
?- flagaPolska([b,b,b,c,c]).
true ;
false.

?- flagaPolska([b,b,c,c,c]).
true ;
false.

?- flagaPolska([b,c,b,c]).
false.

?- flagaPolska([]).
false.

?- flagaPolska([b,c]).
true.

?- flagaPolska([b]).
false.

?- flagaPolska([c]).
false.

?- flagaPolska([b,b,c,c]).
true ;
false.
*/

% i) ew. flagaHolenderska(ListaRWB, RWB) (flaga: red-white-blue)
% uwaga - tutaj zrobilem tak ze pusta lista jest traktowana jako poprawna flaga
flagaHolenderska(ListaRWB, RWB) :- 
	holandiaA(ListaRWB, [], [], [], RWB).
holandiaA([], [], [], [], []).
holandiaA([r|T], R, W, B, RWB) :-
	holandiaA(T, [r|R], W, B, RWB).
holandiaA([w|T], R, W, B, RWB) :-
	holandiaA(T, R, [w|W], B, RWB).
holandiaA([b|T], R, W, B, RWB) :-
	holandiaA(T, R, W, [b|B], RWB).
holandiaA([], [r|RT], W, B, [r|RWBT]) :-
	holandiaA([], RT, W, B, RWBT).
holandiaA([], [], [w|WT], B, [w|RWBT]) :-
	holandiaA([], [], WT, B, RWBT).
holandiaA([], [], [], [b|B], [b|RWBT]) :-
	holandiaA([], [], [], B, RWBT).
/*
?- flagaHolenderska([b,r,b,w,w,b,r,w,r,r,r,b], FH).
FH = [r, r, r, r, r, w, w, w, b|...] ;
false.

?- flagaHolenderska([w,w,b,r,w,r,r,r,b], FH).
FH = [r, r, r, r, w, w, w, b, b] ;
false.

?- flagaHolenderska([], FH).
FH = [] ;
false.
*/

% j) quickSort(L, S) wtw, gdy S jest wynikiem sortowania L (algorytm QuickSort)
% wersja bez akumulatora
% wersja z akumulatorem (czyli bez append) - nie czuje potrzeby robic, ale zrobilbym to tak ze napisalbym wlasny append, nie wiem czy to o to chodzilo
quickSort([H|T], S) :-
	podziel(T, H, SmallerThanH, GreaterOrEqualH),
	quickSort(SmallerThanH, SmallerSorted),
	quickSort(GreaterOrEqualH, GreaterSorted),
	append(SmallerSorted, [H|GreaterSorted], S).
quickSort([], []).
podziel([], _, [], []).
podziel([H|T], X, [H|S], G) :-
	podziel(T, X, S, G),
	H < X.
podziel([H|T], X, S, [H|G]) :-
	podziel(T, X, S, G),
	H >= X.
/*
?- quickSort([5,6,67,78,9,9,10], S).
S = [5, 6, 9, 9, 10, 67, 78] ;
false.

?- quickSort([], S).
S = [].

?- quickSort([-5,-3,-1,0], S).
S = [-5, -3, -1, 0].

?- quickSort([5,3,1,0], S).
S = [0, 1, 3, 5] ;
false.
*/

% k) flatten(L, F) wtw, gdy L jest zagnieżdżoną listą list, których elementami są liczby całkowite, a F jest spłaszczoną listą L (np. flatten([1,[[[[2,[3]]], 4], 5]], [1,2,3,4,5]) - sukces)
flatten([], []).
flatten([H|T], F) :-
	is_list(H),
	flatten(H, FlatHead),
	flatten(T, FlatTail),
	append(FlatHead, FlatTail, F).
flatten([H|T], F) :-
	not(is_list(H)),
	flatten(T, FlatTail),
	append([H], FlatTail, F).
/*
Nie potrafilem sobie poradzic bez uzywania is_list, mialem taka sytuacje:

?- flatten([1,[[[[2,[3]]], 4], [[[]]], 5]], F).
F = [1, 2, 3, 4, 5] ;
F = [1, 2, 3, 4, [], 5] ;
F = [1, 2, 3, 4, [[]], 5] ;
F = [1, 2, 3, 4, [[[]]], 5] ;
F = [1, 2, [3], 4, 5] ;
F = [1, 2, [3], 4, [], 5] ;
F = [1, 2, [3], 4, [[]], 5] ;
F = [1, 2, [3], 4, [[[]]], 5] ;
F = [1, [2, [3]], 4, 5] ;
F = [1, [2, [3]], 4, [], 5] ;
F = [1, [2, [3]], 4, [[]], 5] ;
F = [1, [2, [3]], 4, [[[]]], 5] ;
F = [1, [[2, [3]]], 4, 5] ;
F = [1, [[2, [3]]], 4, [], 5] ;
F = [1, [[2, [3]]], 4, [[]], 5] ;
F = [1, [[2, [3]]], 4, [[[]]], 5] ;
F = [1, [[[2, [3]]], 4], 5] ;
F = [1, [[[2, [3]]], 4], [], 5] ;
F = [1, [[[2, [3]]], 4], [[]], 5] ;
F = [1, [[[2, [3]]], 4], [[[]]], 5] ;
F = [1, [[[[2, [3]]], 4], [[[]]], 5]].

Po dodaniu is_list/1 i not(is_list/1) udalo sie to ograniczyc do jedynego slusznego wyniku.
Daje sie to jakos zrobic bez takiego manewru? Nie wiem czy byl on tutaj dozwolony.

?- flatten([1,[[[[2,[3]]], 4], [[[]]], 5]], F).
F = [1, 2, 3, 4, 5] ;
false.
*/
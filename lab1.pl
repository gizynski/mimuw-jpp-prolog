/* Dana relacja bycia dzieckiem, czyli relacja 3-argumentowa zachodząca między dzieckiem, jego matką i jego ojcem */
/* dziecko(Dziecko, Matka, Ojciec) */ 

dziecko(jasio, ewa, jan).
dziecko(stasio, ewa, jan).
dziecko(basia, anna, piotr).
dziecko(jan, ela, jakub).

ojciec(Dziecko, Ojciec) :- dziecko(Dziecko, _, Ojciec).
matka(Dziecko, Matka) :- dziecko(Dziecko, Matka, _).

rodzic(Dziecko, Rodzic) :- dziecko(Dziecko, Rodzic, _).
rodzic(Dziecko, Rodzic) :- dziecko(Dziecko, _, Rodzic).

babcia(Dziecko, Babcia) :- rodzic(Dziecko, Rodzic), matka(Rodzic, Babcia).
dziadek(Dziecko, Dziadek) :- rodzic(Dziecko, Rodzic), ojciec(Rodzic, Dziadek).

wnuk(Wnuk, KtosZDziadkow) :- babcia(Wnuk, KtosZDziadkow).
wnuk(Wnuk, KtosZDziadkow) :- dziadek(Wnuk, KtosZDziadkow).

przodek(Przodek, Potomek) :- rodzic(Potomek, Przodek).
przodek(Przodek, Potomek) :- rodzic(Potomek, Rodzic), przodek(Przodek, Rodzic).

/* Rozszerzenie danych o dzieciach */
/* dziecko(Dziecko, Matka, Ojciec, płeć) */

dziecko_plec(jasio, ewa, jan, ch).
dziecko_plec(stasio, ewa, jan, ch).
dziecko_plec(basia, anna, piotr, dz).
dziecko_plec(jan, ela, jakub, ch).

chlopiec(Dziecko) :- dziecko_plec(Dziecko, _, _, ch).
dziewczynka(Dziecko) :- dziecko_plec(Dziecko, _, _, dz).

ojciec_plec(Dziecko, Ojciec) :- dziecko_plec(Dziecko, _, Ojciec, _).
matka_plec(Dziecko, Matka) :- dziecko_plec(Dziecko, Matka, _, _).

rodzic_plec(Dziecko, Rodzic) :- dziecko_plec(Dziecko, Rodzic, _, _).
rodzic_plec(Dziecko, Rodzic) :- dziecko_plec(Dziecko, _, Rodzic, _).

dziecko_bez_plci(Dziecko, Matka, Ojciec) :- dziecko_plec(Dziecko, Matka, Ojciec, _).

syn(Dziecko, Matka, Ojciec) :- dziecko_plec(Dziecko, Matka, Ojciec, ch).
corka(Dziecko, Matka, Ojciec) :- dziecko_plec(Dziecko, Matka, Ojciec, dz).

babcia_plec(Dziecko, Babcia) :- rodzic_plec(Dziecko, Rodzic), matka_plec(Rodzic, Babcia).
dziadek_plec(Dziecko, Dziadek) :- rodzic_plec(Dziecko, Rodzic), ojciec_plec(Rodzic, Dziadek).

wnuczek(Dziecko, BabciaDziadek) :- chlopiec(Dziecko), babcia_plec(Dziecko, BabciaDziadek).
wnuczek(Dziecko, BabciaDziadek) :- chlopiec(Dziecko), dziadek_plec(Dziecko, BabciaDziadek).

wnuczka(Dziecko, BabciaDziadek) :- dziewczynka(Dziecko), babcia_plec(Dziecko, BabciaDziadek).
wnuczka(Dziecko, BabciaDziadek) :- dziewczynka(Dziecko), dziadek_plec(Dziecko, BabciaDziadek).

/* Operacje na liczbach naturalnych */

nat(0).
nat(X) :-
    X > 0,
    succ(Y, X),
    nat(Y).
% jak nie zrobie warunku na X > 0, to dla np. -7 rzuci bledem bo succ wymaga nieujemnych wartosci

plus(X, Y, Z) :-
    Z is X + Y.
% o to chodzilo? Czy o rekurencyjne rozwiazanie gdzie stopem jest plus(X, 0, X).?

minus(X, Y, Z) :-
    Z is X - Y.

/* Ciąg Fibonacciego */

fib(1,1).
fib(2,1).

fib(K, N) :-
    K > 2,
    Kmin1 is K - 1,
    Kmin2 is K - 2,
    format('(~w, N): Kmin1=~w, Kmin2=~w~n', [K, N, Kmin1, Kmin2]),
    fib(Kmin1, N1),
    fib(Kmin2, N2),
    N is N1 + N2,
    format('(~w, ~w): Mam odpowiedz!~n', [K, N]).

% ------- zadania z listami ------------

% a) lista(L) wtw, gdy L jest (prologową) listą
lista([]).
lista([_|_]).

% b) pierwszy(E, L) wtw, gdy E jest pierwszym elementem L
pierwszy(H, [H|_]).

% c) ostatni(E, L) wtw, gdy E jest ostatnim elementem L
ostatni(H, [H|[]]).
ostatni(E, [_|Tail]) :- ostatni(E, Tail).

% d) element(E, L) wtw, gdy E jest (dowolnym) elementem L
%    (czyli member/2)
element(E, [E|_]).
element(E, [_|Tail]) :- element(E, Tail).

% e) scal(L1, L2, L3) wtw, gdy L3 = konkatenacja listy L1 z L2
%    (czyli append/3);
%    porównać z definicją funkcji ++ w Haskellu,
%    podać (wiele) zastosowań procedury scal/3
scal([], L2, L2).
scal([H|T], L2, [H|L3]) :- 
    scal(T, L2, L3).

% e') intersect(Z1,Z2) wtw, gdy zbiory (listy) Z1 i Z2 mają niepuste przecięcie
intersect([H1|_], L2) :- element(H1, L2).
intersect([_|T1], L2) :- intersect(T1, L2).

% f) podziel(Lista, NieParz, Parz) == podział danej listy na dwie
%    podlisty zawierające kolejne elementy (odpowiednio) z parzystych
%    (nieparzystych) pozycji
%    (np. podziel([1,3,5,7,9], [1,5,9], [3,7]) - sukces)
podziel([], [], []).
podziel([H|T], [H|NieParz], Parz) :-
    1 is H mod 2,
    podziel(T, NieParz, Parz).
podziel([H|T], NieParz, [H|Parz]) :-
    0 is H mod 2,
    podziel(T, NieParz, Parz).

% g) podlista(P, L) wtw, gdy P jest spójną podlistą L
%    nie wiem co to znaczy spójna podlista, próbowałem znaleźć informację ale bez powodzenia
%    zgaduję że chodzi o to żeby od momentu jak zaczną się pojawiać elementy P na L to wszystkie pojawiły
%    się dokładnie w tej kolejności i bez przerwy żadnej, czyli np. [1,2,3] i [5,3,1,1,2,3,12,4,3]
scisla_podlista([], _).
scisla_podlista([H|T1], [H|T2]) :- scisla_podlista(T1, T2).
podlista(P, L) :- scisla_podlista(P, L).
podlista([H1|T1], [_|T2]) :- podlista([H1|T1], T2).
/* Przyznam ze nie wiem jak to zrobic w jednej klauzuli. To co zrobilem dziala tak:

?- podlista(P, [1,2,3,4]).
P = [] ;
P = [1] ;
P = [1, 2] ;
P = [1, 2, 3] ;
P = [1, 2, 3, 4] ;
P = [2] ;
P = [2, 3] ;
P = [2, 3, 4] ;
P = [3] ;
P = [3, 4] ;
P = [4] ;
false.
*/

% h) podciag(P, L)  wtw, gdy P jest podciągiem L
%    (czyli niekoniecznie spójną podlistą)
%    (preferowane rozwiązanie: każdy podciąg wygenerowany jeden raz) 
podciag([], _).
podciag([H|T], [H|L]) :- podciag(T, L).
podciag([H|T], [_|L]) :- podciag([H|T], L).
/* dziala to tak:

?- podciag(P, [1,2,3,4]).
P = [] ;
P = [1] ;
P = [1, 2] ;
P = [1, 2, 3] ;
P = [1, 2, 3, 4] ;
P = [1, 2, 4] ;
P = [1, 3] ;
P = [1, 3, 4] ;
P = [1, 4] ;
P = [2] ;
P = [2, 3] ;
P = [2, 3, 4] ;
P = [2, 4] ;
P = [3] ;
P = [3, 4] ;
P = [4] ;
false.
*/

% i) wypisz(L) == czytelne wypisanie elementów listy L, z zaznaczeniem
%    jeśli lista pusta (np. elementy oddzielane przecinkami, po
%    ostatnim elemencie kropka)
wypisz([]) :- format('[]~n').
wypisz([H|T]) :- 
    format('[~w', [H]),
    wypisz_ogon(T).
wypisz_ogon([]) :-
    format(']~n').
wypisz_ogon([H|T]) :-
    format(', ~w', [H]),
    wypisz_ogon(T).
/*
?- wypisz([]).
[]
true.

?- wypisz([1,2,43,5]).
[1, 2, 43, 5]
true.
*/

% j) sortowanie przez wstawianie:
%    insertionSort(Lista, Posortowana),
%    insert(Lista, Elem, NowaLista)
insertionSort([], []).
insertionSort([H|T], AllSorted) :- 
    insertionSort(T, TailSorted),
    insert(TailSorted, H, AllSorted).
insert([], A, [A]).
insert([H|T], A, [A,H|T]) :- A =< H.
insert([H|T], A, [H|InsertedInTheTail]) :-
    A > H,
    insert(T, A, InsertedInTheTail).
/* dziala tak:

?- insertionSort([81,1,-2,43,0,5], L).
L = [-2, 0, 1, 5, 43, 81] ;
false.
*/

% k) zadanie domowe:
%       srodek(E, L) wtw, gdy E jest środkowym elementem L
%       (lista nieparzystej długości; np. srodek(3,[1,2,3,4,5]))
%    Uwagi:
%      - w tym zadaniu nie używamy jeszcze arytmetyki (nie trzeba)
%      - możliwe rozwiązania zarówno deklaratywne, jak i imperatywne (tylko jako
%        ćwiczenie) o dużym koszcie
%      - poszukiwane rozwiązanie o koszcie liniowym.

% to sie chyba kwalifikuje jako imperatywne rozwiazanie o koszcie liniowym, obliczam pozycje elementu srodkowego i zwracam jego wartosc
srodek(E, L) :-
    dlugosc(L, D),
    1 is D mod 2,
    P is (D div 2) + 1,
    wez_element(P, L, E).
wez_element(1, [H|_], H).
wez_element(Indeks, [_  |T], Element) :-
    Indeks > 1,
    NowyIndeks is Indeks - 1,
    wez_element(NowyIndeks, T, Element).
dlugosc([], 0).
dlugosc([_|T], D) :-
    dlugosc(T, DlugoscOgona),
    D is DlugoscOgona + 1.

% rozwiazanie drugie, tutaj po kolei sprawdzam kazdy element czy on przypadkiem nie jest srodkiem, koszt kwadratowy
srodek2(E, L) :- 
    dlugosc(L, Dlugosc),
    1 is Dlugosc mod 1,
    indeks(E, L, Indeks),
    Indeks is (Dlugosc div 2) + 1.
indeks(E, [E|_], 1).
indeks(E, [H|T], Indeks) :-
    E \== H,
    indeks(E, T, IndeksWOgonie),
    Indeks is IndeksWOgonie + 1.

% och, po kilku probach udalo mi sie napisac alternatywne obliczanie dlugosci
% teraz trzeba je przerobic tak zeby oddalo element jesli jest na srodkowej pozycji
dlugosc2([], Dlugosc, Pozycja) :-
    Dlugosc is Pozycja - 1.
dlugosc2([_|T], Dlugosc, Pozycja) :-
    PozycjaNastepnego is Pozycja + 1,
    dlugosc2(T, Dlugosc, PozycjaNastepnego).

% no to podejscie kolejne
dlugosc3([], Dlugosc, Pozycja) :-
    Dlugosc is Pozycja - 1,
    wypisz([]),
    format(' ma dlugosc ~w~n', [Dlugosc]).
dlugosc3([H|T], Dlugosc, Pozycja) :-
    PozycjaNastepnego is Pozycja + 1,
    dlugosc3(T, Dlugosc, PozycjaNastepnego),
    wypisz([H|T]),
    format(' ma dlugosc ~w~n', [Dlugosc]).

% trzecie rozwiazanie szukania srodka
srodek3(E, L) :- szukaj_srodka(L, _, 1, _, E).
szukaj_srodka([], Dlugosc, Pozycja, SrodkowaPozycja, Srodek) :-
    Dlugosc is Pozycja - 1,
    1 is Dlugosc mod 2,
    SrodkowaPozycja is (Dlugosc div 2) + 1,
    Srodek = 'ERROR!',
    nl,
    wypisz([]),
    format('    Dlugosc=~w, Pozycja=~w, Srodkowa=~w, Srodek=~n', [Dlugosc, Pozycja, SrodkowaPozycja, Srodek]).
szukaj_srodka([H|T], Dlugosc, Pozycja, SrodkowaPozycja, NowySrodek) :-
    PozycjaNastepnego is Pozycja + 1,
    szukaj_srodka(T, Dlugosc, PozycjaNastepnego, SrodkowaPozycja, Srodek),
    Pozycja \== SrodkowaPozycja,
    NowySrodek = Srodek,
    nl,
    wypisz([H|T]),
    format('    Dlugosc=~w, Pozycja=~w, Srodkowa=~w, H=~w, Srodek=~w~n', [Dlugosc, Pozycja, SrodkowaPozycja, H, NowySrodek]).
szukaj_srodka([H|T], Dlugosc, Pozycja, SrodkowaPozycja, NowySrodek) :-
    PozycjaNastepnego is Pozycja + 1,
    szukaj_srodka(T, Dlugosc, PozycjaNastepnego, SrodkowaPozycja, _),
    Pozycja == SrodkowaPozycja,
    NowySrodek = H,
    nl,
    wypisz([H|T]),
    format('    Dlugosc=~w, Pozycja=~w, Srodkowa=~w, H=~w, Srodek=~w~n', [Dlugosc, Pozycja, SrodkowaPozycja, H, NowySrodek]).

% oczyszczone z wypisywania trzecie rozwiazanie
srodek3_1(E, L) :- szukaj_srodka_1(L, _, 1, _, E).
szukaj_srodka_1([], Dlugosc, Pozycja, SrodkowaPozycja, Srodek) :-
    Dlugosc is Pozycja - 1,
    1 is Dlugosc mod 2,
    SrodkowaPozycja is (Dlugosc div 2) + 1,
    Srodek = 'ERROR!'.
szukaj_srodka_1([_|T], Dlugosc, Pozycja, SrodkowaPozycja, NowySrodek) :-
    PozycjaNastepnego is Pozycja + 1,
    szukaj_srodka_1(T, Dlugosc, PozycjaNastepnego, SrodkowaPozycja, Srodek),
    Pozycja \== SrodkowaPozycja,
    NowySrodek = Srodek.
szukaj_srodka_1([H|T], Dlugosc, Pozycja, SrodkowaPozycja, NowySrodek) :-
    PozycjaNastepnego is Pozycja + 1,
    szukaj_srodka_1(T, Dlugosc, PozycjaNastepnego, SrodkowaPozycja, _),
    Pozycja == SrodkowaPozycja,
    NowySrodek = H.
/* dziala to tak:
?- srodek3_1(E,[81,1,7,-2,43,0,5]).
E = -2 .
*/

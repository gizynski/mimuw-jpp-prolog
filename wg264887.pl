%% Wojciech Giżyński, 264887
%% 
%% Automat reprezentuję jako listę akcji - taki odpowiednik tabelki dla LR(0).
%% Możliwe są trzy akcje: shift, reduce i accept. Każdy stan ma dokładnie jeden
%% typ akcji, dlatego w stanie trzymam informację o jego typie. Tam też szukam
%% ew. konfliktów - konflikt('shift/reduce') i konflikt('reduce/reduce').
%% W takiej formie są też przekazywane w Info w przypadku gramatyki nie-LR(0).
%% Nie umieszczałem więcej informacji, bo tak zrozumiałem treść zadania.
%% Akcję accept tworzę dla stanu typu reduce z regułą Z->S#.

%% Przepraszam, ale nie miałem możliwości przetestowania programu na maszynie
%% students. Wynika to z tego że nie mam do niej dostępu w tym semestrze.
%% Pisałem pod SWI-Prolog (threaded, 64 bits, version 7.4.2).
%% Zainstalowałem i testowałem też na SICStus 4.3.5 (x86_64-linux-glibc2.5).

%% ----------------------------------------------------------------------------
%% Dla przekazanej gramatyki tworzony jest automat (pełna lista akcji).
%% Możliwe akcje to reduce, shift i accept. Tworzenie automatu odbywa się za
%% pomocą przetwarzania utworzonych stanów i dodawaniu kolejnych gdy trzeba.
%%
%% createLR(+Gramatyka, -Automat, -Info)

createLR(Gramatyka, Automat, Info) :-
    rulesList(Gramatyka, RegulyGramatyki),
    RegulyGramatyki = [RegulaStartowa|_],
    closure(RegulyGramatyki, [RegulaStartowa], RozwiniecieStartowejReguly),
    createState(0, RozwiniecieStartowejReguly, StanStart),
    process(RegulyGramatyki, [StanStart], [StanStart], [], [], Automat, Info).

%% ----------------------------------------------------------------------------
%% Sprawdzanie czy automat akceptuje dane słowo, korzystając ze stosu
%% na którym umieszczane są naprzemiennie numery stanów i symbole.
%% Dla aktualnego stanu szukana jest pasująca akcja i wykonywana.
%% 
%% accept(+Automat, +Slowo)

accept(Automat, Slowo) :-
    accepts(Automat, Slowo, [0]).

%% accepts(+ListaAkcjiAutomatu, +PozostalaCzescSlowa, +StosAnalizy)

accepts(Akcje, [], [NumerStanu|_]) :-
    member(shift(NumerStanu, '#', NowyStan), Akcje), !,
    member(accept(NowyStan), Akcje).
accepts(Akcje, ResztaSlowa, [NumerStanu|Stos]) :-
    member(reduce(NumerStanu, IleSymboli, Nieterminal), Akcje), !,
    WielkoscRedukcji is 2 * IleSymboli - 1,
    dropFirstN(WielkoscRedukcji, Stos, NowyStos),
    accepts(Akcje, [Nieterminal|ResztaSlowa], NowyStos).
accepts(Akcje, [Symbol|ResztaSymboli], [NumerStanu|Stos]) :-
    member(shift(NumerStanu, Symbol, NowyStan), Akcje), !,
    accepts(Akcje, ResztaSymboli, [NowyStan, Symbol, NumerStanu|Stos]).

dropFirstN(IleElementow, OryginalnaLista, SkroconaLista) :-
    length(Odciecie, IleElementow),
    append(Odciecie, SkroconaLista, OryginalnaLista).

%% ----------------------------------------------------------------------------
%% Zamiana przekazanej gramatyki na listę pojedynczych reguł, dla wygody.
%% List zawiera też dodatkową regułę startową Z->.S#, gdzie S to startowy nt.
%% Reguły są postaci regula(NazwaNieterm, Pozycja, SkladnikiProdukcji)
%% 
%% rulesList(+Gramatyka, -ListaUtworzonychRegul)

rulesList(Gramatyka, ListaRegul) :-
    Gramatyka = gramatyka(SymbolPoczatkowy, ZbiorProdukcji),
    DodatkowaProdukcjaStartowa = prod('Z', [[nt(SymbolPoczatkowy), '#']]),
    PoszerzonyZbiorProd = [DodatkowaProdukcjaStartowa|ZbiorProdukcji],
    rulesList(PoszerzonyZbiorProd, ListaRegul).

%% rulesList(+ProdukcjeDoPrzetworzenia, -ListaUtworzonychRegul)

rulesList([], []).
rulesList([AnalizowanaProdukcja|ResztaProdukcji], ListaRegul) :-
    AnalizowanaProdukcja = prod(NazwaNieterminala, ListaPrawychStronProdukcji),
    makeRules(NazwaNieterminala, ListaPrawychStronProdukcji, PojedynczeReguly),
    rulesList(ResztaProdukcji, ListaRegulOgona),
    append(PojedynczeReguly, ListaRegulOgona, ListaRegul).

%% Zamiana produkcji dla danego nieterminala na pojedyncze reguły.
%% makeRules(+NazwaNieterminala, +ListaPrawychStronProd, -PojedynczeReguly)

makeRules(_, [], []).
makeRules(NazwaNieterminala, [Produkcja|ResztaProdukcji], ListaRegul) :-
    ListaRegul = [regula(NazwaNieterminala, 0, Produkcja)|ResztaRegul],
    makeRules(NazwaNieterminala, ResztaProdukcji, ResztaRegul).

%% ----------------------------------------------------------------------------
%% Rozwijanie początkowych reguł do pełnej listy, szykując je dla stanu.
%% Rozwijanie polega na dodaniu reguł powiązanych z nieterminalem za kropką.
%% Na koniec reguły są sortowane, żeby porównywanie stanów było możliwe.
%% 
%% closure(+BazaRegulGramatyki, +PoczatkoweReguly, -RozwinietaListaRegulPocz)

closure(Reguly, Startowe, Wynik) :-
    closureA(Reguly, Startowe, [], Rozwiniete),
    sort(Rozwiniete, Wynik).

%% Pomocniczy predykat rozszerzający listę reguł o reguły od nieterminali.
%% closureA(+Reguly, +KolejkaRegul, +RozwinieteNT, +RozwinietaListaRegul)

closureA(_, [], _, []).
closureA(Reguly, [Regula|Kolejka], RozwNT, [Regula|ResztaRegul]) :-
    nextItem(Regula, ElemPoKropce), !,
    expandNT(ElemPoKropce, Reguly, RozwNT, Kolejka, NoweRozwNT, NowaKolejka),
    closureA(Reguly, NowaKolejka, NoweRozwNT, ResztaRegul).
closureA(Reguly, [Regula|Kolejka], RozwNT, [Regula|ResztaRegul]) :-
    \+ nextItem(Regula, _),
    closureA(Reguly, Kolejka, RozwNT, ResztaRegul).

%% nextItem(+Regula, -ElementPoKropce)

nextItem(regula(_, Pozycja, Elementy), ElementPoKropce) :-
    nth0(Pozycja, Elementy, ElementPoKropce).

%% expandNT(+Element, +Reguly, +RozwNT, +Kolejka, -NoweRozwNT, -NowaKolejka)

expandNT(Element, _, RozwNT, Kolejka, RozwNT, Kolejka) :-
    (\+ Element = nt(_) ; Element = nt(NT), member(NT, RozwNT)), !.
expandNT(nt(NT), Reguly, RozwNT, Kolejka, [NT|RozwNT], NowaKolejka) :-
    \+ member(NT, RozwNT),
    expandedNT(NT, Reguly, RegulyRozwiniecia),
    append(RegulyRozwiniecia, Kolejka, NowaKolejka).

%% Zebranie rozwinięcia wszystkich reguł dla danego nieterminala.
%% expandedNT(+Nieterminal, +WszystkieReguly, -RegulyRozwinieciaNT)

expandedNT(_, [], []).
expandedNT(Nieterminal, [Regula|PozostaleReguly], RozwiniecieNT) :-
    Regula = regula(X, _, Elementy),
    expandedNT(Nieterminal, PozostaleReguly, RozwOgona),
    (Nieterminal = X -> 
        RozwiniecieNT = [regula(Nieterminal, 0, Elementy)|RozwOgona] ;
        RozwiniecieNT = RozwOgona).

%% ----------------------------------------------------------------------------
%% Analiza stanu i tworzenie listy "generatorów" dzięki której powstaną kolejne 
%% stany z zadanej początkowej listy reguł. Będą rozszerzane closure do pełna.
%% Ten zabieg jest używany w przypadku stanu typu shift.
%% 
%% stateGenerators(+StanStartowyTypuShift, -GeneratoryWynikowe)

stateGenerators(Stan, Generatory) :-
    Stan = stan(Id, ListaRegul, shift),
    shiftGenerators(Id, ListaRegul, [], Generatory).

%% Pomocniczy predykat z akumulatorem do zebrania generatorów
%% shiftGenerators(+IdStanu, +RegulyStanu, +AkumulatorGen -Generatory)

shiftGenerators(_, [], Generatory, GeneratoryPolaczone) :-
    sort(Generatory, GeneratoryPosortowane),
    joinGenerators(GeneratoryPosortowane, [], GeneratoryPolaczone).
shiftGenerators(IdStanu, [Regula|T], GeneratoryAcc, Generatory) :-
    Regula = regula(NT, Pozycja, Elementy),
    nextItem(Regula, E),
    PozycjaPlus1 is Pozycja + 1,
    NowyGen = generator(IdStanu, E, regula(NT, PozycjaPlus1, Elementy)),
    shiftGenerators(IdStanu, T, [NowyGen|GeneratoryAcc], Generatory).

%% Połącz generatory, jeśli dotyczą shift dla tego samego symbolu.
%% joinGenerators(+PosortowaneGeneratory, +AkumulatorGen -GotoweGeneratory)

joinGenerators([], Generatory, Generatory).
joinGenerators([generator(Id, E, R)|T], [], Generatory) :-
    joinGenerators(T, [generator(Id, E, [R])], Generatory).
joinGenerators([generator(Id, E, R)|T], [AH|AT], Generatory) :-
    (AH = generator(Id, E, ListR) -> 
        NowyAku = [generator(Id, E, [R|ListR])|AT] ; 
        NowyAku = [generator(Id, E, [R]), AH|AT]), 
    joinGenerators(T, NowyAku, Generatory).

%% ----------------------------------------------------------------------------
%% Tworzenie stanów z listy reguł, badając typ stanu i wykrywając konflikty.
%% 
%% createState(+IdDoNadaniaStanowi, +PelnaListaRegulStanu, -Stan)

createState(Id, ListaRegul, Stan) :-
    stateType(ListaRegul, [], [], Typ),
    Stan = stan(Id, ListaRegul, Typ).

%% Policz reguły shift i reduce, a z wyników określ jakiego rodzaju jest stan.
%% stateType(+ListaRegul, +ListaRegulShift, +ListaRegulReduce, -TypStanu)

stateType([], [], [_|[]], reduce).
stateType([], [], [_,_|_], reduce_reduce).
stateType([], [_|_], [], shift).
stateType([], [_|_], [_|_], shift_reduce).
stateType([H|T], LS, LR, Typ) :-
    H = regula(_, Pozycja, Elementy),
    (length(Elementy, Pozycja) -> 
        stateType(T, LS, [H|LR], Typ) ; 
        stateType(T, [H|LS], LR, Typ)).

%% ----------------------------------------------------------------------------
%% Serce programu. Przetwarza kolejkę stanów, zajmując się każdym według jego
%% typu. Dla stanów typu shift ustala generatory i tworzy z nich nowe stany.
%% Dla wszystkich stanów dodaje odpowiednie akcje do bazy akcji, które później
%% będą stanowiły wynikowy automat. W przypadku konfliktu, przekazuje info.
%% 
%% process(+Reguly, +Stany, +KolStan, +KolGen, +AkuAkcji, -Akcje, -Info).

process(_, _, [], [], Akcje, Akcje, yes).
process(_, _, [Stan|_], _, _, null, konflikt('shift/reduce')) :-
    Stan = stan(_, _, shift_reduce), !.
process(_, _, [Stan|_], _, _, null, konflikt('reduce/reduce')) :-
    Stan = stan(_, _, reduce_reduce), !.
process(Reguly, Stany, [S|T], KolejkaGen, AkcjeAku, Akcje, Info) :-
    S = stan(_, _, shift), !,
    stateGenerators(S, GeneratoryZeStanu),
    append(GeneratoryZeStanu, KolejkaGen, NowaKolejkaGen),
    process(Reguly, Stany, T, NowaKolejkaGen, AkcjeAku, Akcje, Info).
process(Reguly, Stany, [S|T], KolejkaGen, AkcjeAku, Akcje, Info) :-
    S = stan(Id, [regula(NazwaNieterminala, Pozycja, _)|[]], reduce), 
    NazwaNieterminala \= 'Z', !,
    NowyAkcjeAku = [reduce(Id, Pozycja, nt(NazwaNieterminala))|AkcjeAku],
    process(Reguly, Stany, T, KolejkaGen, NowyAkcjeAku, Akcje, Info).
process(Reguly, Stany, [S|T], KolejkaGen, AkcjeAku, Akcje, Info) :-
    S = stan(Id, [regula('Z', 2, _)|[]], reduce), !,
    NowyAkcjeAku = [accept(Id)|AkcjeAku],
    process(Reguly, Stany, T, KolejkaGen, NowyAkcjeAku, Akcje, Info).
process(Reguly, Stany, [], [G|T], AkcjeAku, Akcje, Info) :-
    G = generator(IdStartu, Symbol, RegulyDoRozwiniecia),
    closure(Reguly, RegulyDoRozwiniecia, RozwReguly),
    addUniqueState(Stany, RozwReguly, NoweStany, IdCelu),
    append(NoweStany, Stany, NowyZbiorStanow),
    NowyAkcjeAku = [shift(IdStartu, Symbol, IdCelu)|AkcjeAku],
    process(Reguly, NowyZbiorStanow, NoweStany, T, NowyAkcjeAku, Akcje, Info).

%% Dodaj stan, chyba że taki już istnieje. Zwraca Id (może nowego) stanu.
%% addUniqueState(+Stany, +RozwReguly, -NowyZbiorStanow, -IdStanu)

addUniqueState(Stany, RozwinieteReguly, [], IdIstniejacegoS) :-
    member(stan(IdIstniejacegoS, RozwinieteReguly, _), Stany), !.
addUniqueState(Stany, RozwinieteReguly, [NowyStan], IdNowegoStanu) :-
    \+ member(stan(_, RozwinieteReguly, _), Stany), !,
    newStateId(Stany, IdNowegoStanu),
    createState(IdNowegoStanu, RozwinieteReguly, NowyStan).

%% Ustal jaki numer Id powinien otrzymać następny utworzony stan.
%% newStateId(+BazaStanow, -NumerIdDlaNowegoStanu)

newStateId([NajnowszyStan|_], Numer) :-
    NajnowszyStan = stan(NajnowszeId, _, _),
    Numer is NajnowszeId + 1.

%% ----------------------------------------------------------------------------



































%% --------------------------------------------------------
%% Wypisywanie gramatyki

wypisz_gramatyke(Gramatyka) :-
    Gramatyka = gramatyka(SymbolPoczatkowy, ZbiorProdukcji),
    format('~n│ GRAMATYKA - symbol poczatkowy: ~w~n│~n', [SymbolPoczatkowy]),
    wypisz_produkcje(ZbiorProdukcji),
    nl.

wypisz_produkcje([]).
wypisz_produkcje([H|T]) :-
    H = prod(NazwaNieterminala, ListaPrawychStronProdukcji),
    format('│ ~w →  ', [NazwaNieterminala]),
    wypisz_prawa_strone_produkcji(ListaPrawychStronProdukcji),
    wypisz_produkcje(T).

wypisz_prawa_strone_produkcji([]).
wypisz_prawa_strone_produkcji([H|T]) :-
    (H = [] -> wypisz_elementy_produkcji(['ε']) ; wypisz_elementy_produkcji(H)),
    (T = [] -> nl ; write(' | ')),
    wypisz_prawa_strone_produkcji(T).

wypisz_elementy_produkcji([]).
wypisz_elementy_produkcji([H|T]) :-
    (H = nt(X) -> write(X) ; write(H)),
    wypisz_elementy_produkcji(T).

%% --------------------------------------------------------
%% Wypisywanie reguł

wypisz_reguly(ListaRegul) :-
    format('~n│ REGUŁY~n│'),
    wypisz_reguly_rekurencyjnie(ListaRegul),
    nl, nl.

wypisz_reguly_rekurencyjnie([]).
wypisz_reguly_rekurencyjnie([H|T]) :-
    format('~n│ '),
    wypisz_regule(H),
    wypisz_reguly_rekurencyjnie(T).

wypisz_regule(regula(NT, P, E)) :-
    %% format('(~w) ~w →  ', [P, NT]),
    format('~w →  ', [NT]),
    %% (E = [] -> write('ε') ; wypisz_elementy_produkcji(E)),
    (E = [] -> wypisz_elementy_reguly(0, P, ['ε']) ; wypisz_elementy_reguly(0, P, E)).

wypisz_elementy_reguly(Pozycja, PozycjaKropki, []) :-
    (Pozycja = PozycjaKropki -> format('.') ; format('')).
wypisz_elementy_reguly(Pozycja, PozycjaKropki, [H|T]) :-
    (Pozycja = PozycjaKropki -> format('.') ; format('')),
    (H = nt(X) -> write(X) ; write(H)),
    NowaPozycja is Pozycja + 1,
    wypisz_elementy_reguly(NowaPozycja, PozycjaKropki, T).

%% --------------------------------------------------------
%% Wypisywanie generatorów

wypisz_generatory(Generatory) :-
    format('~n│ GENERATORY SHIFT~n│'),
    wypisz_generator(Generatory),
    nl.

wypisz_generator([]) :-
    nl.
wypisz_generator([H|T]) :-
    H = generator(Id, E, ListaR),
    (E = nt(X) -> Symbol = X; Symbol = E),
    format('~n│ ze stanu ~w po symbolu ~w', [Id, Symbol]),
    wypisz_reguly_dla_gen(ListaR),
    wypisz_generator(T).

wypisz_reguly_dla_gen([]).
wypisz_reguly_dla_gen([R|T]) :-
    format('~n│       '),
    wypisz_regule(R),
    wypisz_reguly_dla_gen(T).

%% --------------------------------------------------------
%% Wypisywanie stanów

wypisz_stan(Stan) :-
    Stan = stan(Id, ListaRegul, Typ),
    format('~n│ STAN ~w (~w)~n│', [Id, Typ]),
    wypisz_reguly_rekurencyjnie(ListaRegul),
    nl, nl.

%% --------------------------------------------------------
%% Wypisywanie automatu

wypisz_auto(Auto) :-
    format('~n│ AUTOMAT ~n│'),
    wypisz_akcje(Auto).

wypisz_akcje([]).
wypisz_akcje([Akcja|T]) :-
    Akcja = accept(Skad), !,
    format('~n│ ~w akceptuje', [Skad]),
    wypisz_akcje(T).
wypisz_akcje([Akcja|T]) :-
    Akcja = shift(Skad, Jak, Dokad), !,
    format('~n│ ~w →  ~w →  ~w', [Skad, Jak, Dokad]),
    wypisz_akcje(T).
wypisz_akcje([Akcja|T]) :-
    Akcja = reduce(Skad, IleSymboli, nt(NazwaNieterminala)), !,
    format('~n│ ~w redukuj ~w symboli do ~w', [Skad, IleSymboli, NazwaNieterminala]),
    wypisz_akcje(T).
%% --------------------------------------------------------
%% Gramatyki do testów

grammar(ex1, gramatyka('E', [prod('E', [[nt('E'), '+', nt('T')], [nt('T')]]), prod('T', [[id], ['(', nt('E'), ')']])])).
grammar(ex2, gramatyka('A', [prod('A', [[nt('A'), x], [x]])])).
grammar(ex3, gramatyka('A', [prod('A', [[x, nt('A')], [x]])])).
grammar(ex4, gramatyka('A', [prod('A', [[x, nt('B')], [nt('B'), y], []]), prod('B', [[]])])).
grammar(ex5, gramatyka('S', [prod('S', [[id], [nt('V'), ':=', nt('E')]]), prod('V', [[id], [id, '[', nt('E'), ']']]), prod('E', [[v]])])).
grammar(ex6, gramatyka('A', [prod('A', [[x], [nt('B'), nt('B')]]), prod('B', [[x]])])).
grammar(ex7, gramatyka('S', [prod('S', [[nt('E')]]), prod('E', [[nt('E'), '+', nt('V')], [nt('V')]]), prod('V', [['x'], ['y']])])).

%% --------------------------------------------------------
%% Testy do rozwiązania

tests([
    %% test_accept(ex1, [id], false),
    test_createLR(ex1, _, yes),
    test_createLR(ex2, _, yes),
    test_createLR(ex3, null, _),
    test_createLR(ex3, null, konflikt('shift/reduce')),
    test_createLR(ex4, null, _),
    test_createLR(ex4, null, konflikt('shift/reduce')),
    test_createLR(ex5, null, _),
    test_createLR(ex5, null, konflikt('shift/reduce')),
    test_createLR(ex6, null, _),
    test_createLR(ex6, null, konflikt('reduce/reduce')),
    test_createLR(ex7, null, _),
    test_createLR(ex7, null, konflikt('shift/reduce')),
    test_accept(ex1, [id], true),
    test_accept(ex1, ['(',id,')'], true),
    test_accept(ex1, [id,'+',ident], false),
    test_accept(ex1, [id,'+',id], true),
    test_accept(ex1, ['(', '(', '(', id, ')', ')', ')'], true),
    test_accept(ex1, ['(', '(', '(', id, ')', ')'], false),
    test_accept(ex1, ['(', '(', id, ')', ')', ')'], false),
    test_accept(ex1, ['(', '(', '(', id, '+', id, ')', ')', ')'], true),
    test_accept(ex1, ['(', '(', '(', id, '+', id, '+', '(', id, ')', ')', ')', ')'], true),
    test_accept(ex1, ['('], false),
    test_accept(ex1, [')'], false),
    test_accept(ex1, ['(', ')'], false),
    
    test_accept(ex2, [], false),
    test_accept(ex2, [x], true),
    test_accept(ex2, [x, x, x, x], true),
    test_accept(ex2, [xx], false),
    test_accept(ex2, [x, xx, x], false)
]).

%% --------------------------------------------------------
%% Framework do uruchamiania i prezentacji wyników testów

test_createLR(NG, OczekiwaneAuto, OczekiwaneInfo) :-
    format('test_createLR dla ~w, oczekuje (~w, ~w)', [NG, OczekiwaneAuto, OczekiwaneInfo]),
    grammar(NG, G),
    G = gramatyka(_, ZbiorProdukcji), nl,
    wypisz_produkcje(ZbiorProdukcji),
    createLR(G, OczekiwaneAuto, Info),
    format('│ Auto created? ~w~n', [Info]),
    Info = OczekiwaneInfo.

test_accept(NG, Slowo, OczekiwanyWynik) :-
    format('test_accept '),
    wypisz_elementy_produkcji(Slowo),
    format(' (formalnie ~w) przez ~w, oczekuje ', [Slowo, NG]),
    (OczekiwanyWynik -> format('TAK') ; format('NIE')),
    grammar(NG, G),
    createLR(G, Automat, yes),
    (accept(Automat, Slowo) -> Wynik = true ; Wynik = false),
    G = gramatyka(_, ZbiorProdukcji), nl,
    wypisz_produkcje(ZbiorProdukcji),
    Wynik = OczekiwanyWynik.

test :- 
    tests(Tests),
    length(Tests, NumberOfTests),
    format('~n│ RUNNING ~w TESTS~n│', [NumberOfTests]),
    run_tests(0, Tests, Score, FailedTests), 
    FailedCount is NumberOfTests - Score,
    (Score \= NumberOfTests -> 
        format('~n~nTESTS FAILED (~w/~w): ~w', [FailedCount, NumberOfTests, FailedTests]) ; 
        format('~n~nTESTS PASSED (~w/~w)', [Score, NumberOfTests])
    ),
    Score = NumberOfTests.

run_tests(_, [], 0, []).
run_tests(Number, [Test|T], Score, Failed) :-
    Id is Number + 1,
    format('~n│ ~w: ', [Id]),
    (Test -> 
        Points = 1, format('│~t[OK]~75|~n│~n│'), Failed = TailFailed; 
        Points = 0, format('│~t[FAILED]~75|~n│~n│'), Failed = [Id|TailFailed]
    ),
    run_tests(Id, T, TailScore, TailFailed),
    Score is TailScore + Points.

%% --------------------------------------------------------
%% Gramatyki z przykladow w zadaniu, ktore na pewno przydadza sie do testowania



gramatyka('E', [prod('E', [[nt('E'), +, nt('T')], [nt('T')]]), prod('T', [[id], ['(', nt('E'), ')']])]).
/*
│ GRAMATYKA - symbol poczatkowy: E
│
│ E -> E+T | T
│ T -> id | (E)

G = gramatyka('E', [prod('E', [[nt('E'), +, nt('T')], [nt('T')]]), prod('T', [[id], ['(', nt('E'), ')']])]) ;
*/

gramatyka('E', [prod('E', [[nt('E'), '+', nt('T')], [nt('T')]]), prod('T', [[id], ['(', nt('E'), ')']])]).
/*
│ GRAMATYKA - symbol poczatkowy: E
│
│ E -> E+T | T
│ T -> id | (E)

G = gramatyka('E', [prod('E', [[nt('E'), +, nt('T')], [nt('T')]]), prod('T', [[id], ['(', nt('E'), ')']])]) ;
*/

gramatyka('A', [prod('A', [[nt('A'), x], [x]])]).
/*
│ GRAMATYKA - symbol poczatkowy: A
│
│ A -> Ax | x

G = gramatyka('A', [prod('A', [[nt('A'), x], [x]])]) ;
*/

gramatyka('A', [prod('A', [[x, nt('A')], [x]])]).
/*
│ GRAMATYKA - symbol poczatkowy: A
│
│ A -> xA | x

G = gramatyka('A', [prod('A', [[x, nt('A')], [x]])]) ;
*/

gramatyka('A', [prod('A', [[x, nt('B')], [nt('B'), y], []]), prod('B', [[]])]).
/*
│ GRAMATYKA - symbol poczatkowy: A
│
│ A -> xB | By | ε
│ B -> ε

G = gramatyka('A', [prod('A', [[x, nt('B')], [nt('B'), y], []]), prod('B', [[]])]) ;
*/

gramatyka('S', [prod('S', [[id], [nt('V'), ':=', nt('E')]]), prod('V', [[id], [id, '[', nt('E'), ']']]), prod('E', [[v]])]).
/*
│ GRAMATYKA - symbol poczatkowy: S
│
│ S -> id | V:=E
│ V -> id | id[E]
│ E -> v

G = gramatyka('S', [prod('S', [[id], [nt('V'), :=, nt('E')]]), prod('V', [[id], [id, '[', nt(...)|...]]), prod('E', [[v]])]) 
*/

gramatyka('A', [prod('A', [[x], [nt('B'), nt('B')]]), prod('B', [[x]])]).
/*
│ GRAMATYKA - symbol poczatkowy: A
│
│ A -> x | BB
│ B -> x

G = gramatyka('A', [prod('A', [[x], [nt('B'), nt('B')]]), prod('B', [[x]])]).
*/
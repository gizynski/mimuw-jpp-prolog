% a) drzewo(D) wtw, gdy D jest drzewem binarnym
drzewo(nil).
drzewo(t(L, _, P)) :-
    drzewo(L),
    drzewo(P).
/*
?- drzewo(t(nil, 1, nil)).
true.

?- drzewo(nil).
true.

?- drzewo(t(t(nil, 1, nil), 2, t(nil, 3, t(nil, 4, nil)))).
true.
*/

% b) insertBST(DrzewoBST, Elem, NoweDrzewoBST)
insertBST(nil, E, t(nil, E, nil)).
insertBST(t(L, X, P), E, t(NoweL, X, P)) :-
    E =< X,
    insertBST(L, E, NoweL).
insertBST(t(L, X, P), E, t(L, X, NoweP)) :-
    E > X,
    insertBST(P, E, NoweP).
/*
?- insertBST(t(t(nil, 1, nil), 2, t(nil, 3, t(nil, 4, nil))), 2, D).
D = t(t(nil, 1, t(nil, 2, nil)), 2, t(nil, 3, t(nil, 4, nil))) ;
false.

?- insertBST(nil, 5, D).
D = t(nil, 5, nil).
*/  

% c) (ew. wypiszBST(D) = wypisanie na ekranie, porządek infiksowy)  
rysD(D) :-
    drzewo(D),
    nl,
    rysujD(D, 0, [], [srodek]),
    nl.
rysujD(nil, _, _, _).

rysujD(t(L, X, P), W, Kreski, [srodek]) :-
    WiekszeW is W + 1,
    rysujD(P, WiekszeW, [lewy, srodek], [prawy, srodek]),
    reverse(Kreski, KreskiOdwr),
    wciecie(W, KreskiOdwr, [srodek]),
    format('~w~n', [X]),
    rysujD(L, WiekszeW, [prawy, srodek], [lewy, srodek]).

rysujD(t(L, X, P), W, Kreski, [K|Historia]) :-
    WiekszeW is W + 1,
    rysujD(P, WiekszeW, [lewy|Kreski], [prawy, K|Historia]),
    reverse([K|Historia], HistoriaOdwr),
    reverse(Kreski, KreskiOdwr),
    wciecie(W, KreskiOdwr, HistoriaOdwr),
    format('~w~n', [X]),
    rysujD(L, WiekszeW, [prawy|Kreski], [lewy, K|Historia]).

wciecie(0, [], [srodek]) :-
    format('────  ').
wciecie(0, [prawy], [lewy]) :-
    format('╰──── ').
wciecie(0, [lewy], [prawy]) :-
    format('╭──── ').
wciecie(W, [lewy,lewy|KreskiT], [_|HistoriaT]) :-
    format('      '),
    MniejszeW is W - 1,
    wciecie(MniejszeW, [lewy|KreskiT], HistoriaT).
wciecie(W, [prawy,prawy|KreskiT], [_|HistoriaT]) :-
    format('      '),
    MniejszeW is W - 1,
    wciecie(MniejszeW, [prawy|KreskiT], HistoriaT).
wciecie(W, [KreskiH|KreskiT], [_|HistoriaT]) :-
    member(KreskiH, HistoriaT),
    format('│     '),
    MniejszeW is W - 1,
    wciecie(MniejszeW, KreskiT, HistoriaT).
wciecie(W, [_|KreskiT], [_|HistoriaT]) :-
    format('      '),
    MniejszeW is W - 1,
    wciecie(MniejszeW, KreskiT, HistoriaT).
/*
?- stworzBST([5,2,213,554,4531,565,4,5,34,3,4,5,35,76,7989,34,-21,-5,-3,5,567,3,4,8], D), rysD(D).

                        ╭──── 7989
                  ╭──── 4531
                  │     │     ╭──── 567
                  │     ╰──── 565
            ╭──── 554
      ╭──── 213
      │     │           ╭──── 76
      │     │     ╭──── 35
      │     ╰──── 34
      │           ╰──── 34
      │                 ╰──── 8
────  5
      │           ╭──── 5
      │           │     ╰──── 5
      │           │           ╰──── 5
      │     ╭──── 4
      │     │     │     ╭──── 4
      │     │     │     │     ╰──── 4
      │     │     ╰──── 3
      │     │           ╰──── 3
      ╰──── 2
            │           ╭──── -3
            │     ╭──── -5
            ╰──── -21

*/

% d) wypiszBST(D, L) wtw, gdy L=lista wszystkich wierzchołków D (porządek infiksowy)
listaBST(nil, []).
listaBST(t(L, X, P), Lista) :-
    listaBST(P, ListaP),
    listaBST(L, ListaL),
    append(ListaL, [X|ListaP], Lista).
/*
?- listaBST(t(t(nil, 1, nil), 2, t(nil, 3, t(nil, 4, nil))), L).
L = [1, 2, 3, 4].

?- listaBST(nil, L).
L = [].
*/

% e) stworzBST(L, D) wtw, gdy D jest drzewem BST zawierającym wszystkie elementy listy L (akumulator, ew. bez)
stworzBST(L, D) :-
    stworzD(L, nil, D).
stworzD([], A, A).
stworzD([H|T], A, D) :-
    insertBST(A, H, NewA),
    stworzD(T, NewA, D).
/*
?- stworzBST([9,7,3,33,6,34,2,14,5,1], D), rysD(D).

            ╭──── 34
      ╭──── 33
      │     ╰──── 14
────  9
      ╰──── 7
            │     ╭──── 6
            │     │     ╰──── 5
            ╰──── 3
                  ╰──── 2
                        ╰──── 1

D = t(t(t(t(t(nil, 1, nil), 2, nil), 3, t(t(nil, 5, nil), 6, nil)), 7, nil), 9, t(t(nil, 14, nil), 33, t(nil, 34, nil))) 
*/

% f) liscie(D, L) wtw, gdy L = lista wszystkich liści, od lewej do prawej
liscie(D, L) :- 
    liscieA(D, L).
liscieA(nil, []).
liscieA(t(nil, X, nil), [X]).
liscieA(t(L, _, nil), AL) :-
    L \== nil,
    liscieA(L, AL).
liscieA(t(nil, _, R), AR) :-
    R \== nil,
    liscieA(R, AR).
liscieA(t(L, _, R), ALR) :-
    R \== nil,
    liscieA(R, AR),
    L \== nil,
    liscieA(L, AL),
    append(AL, AR, ALR).
/*
?- liscie(nil, L).
L = [].

?- liscie(t(nil, X, t(nil, 2, nil)), L).
L = [2] ;
false.

?- stworzBST([9,7,3,33,6,34,2,14,5,1], D), rysD(D), liscie(D,L).

            ╭──── 34
      ╭──── 33
      │     ╰──── 14
────  9
      ╰──── 7
            │     ╭──── 6
            │     │     ╰──── 5
            ╰──── 3
                  ╰──── 2
                        ╰──── 1

D = t(t(t(t(t(nil, 1, nil), 2, nil), 3, t(t(nil, 5, nil), 6, nil)), 7, nil), 9, t(t(nil, 14, nil), 33, t(nil, 34, nil))),
L = [1, 5, 14, 34] .
*/

% g) sortBST(L, S) wtw, gdy S = lista posortowana, przy użyciu drzew BST
sortBST(L, S) :-
    stworzBST(L, D),
    listaBST(D, S).
/*
?- sortBST([9,7,3,33,6,34,2,14,5,1], S).
S = [1, 2, 3, 5, 6, 7, 9, 14, 33|...] ;
false.
*/

% h) wszerz(D, L) wtw, gdy L = lista wszystkich wierzchołków wszerz
wszerzBST(D, L) :-
    wszerz([D], L).
wszerz([], []).
wszerz([t(L,X,R)|T], [X|A]) :-
    append(T, [L,R], Kolejka),
    wszerz(Kolejka, A).
wszerz([nil|T], A) :-
    wszerz(T, A).
/*
?- stworzBST([9,7,3,33,6,34,2,14,5,1], D), rysD(D), wszerzBST(D,L).

            ╭──── 34
      ╭──── 33
      │     ╰──── 14
────  9
      ╰──── 7
            │     ╭──── 6
            │     │     ╰──── 5
            ╰──── 3
                  ╰──── 2
                        ╰──── 1

D = t(t(t(t(t(nil, 1, nil), 2, nil), 3, t(t(nil, 5, nil), 6, nil)), 7, nil), 9, t(t(nil, 14, nil), 33, t(nil, 34, nil))),
L = [9, 7, 33, 3, 14, 34, 2, 6, 1|...] .
*/

% h) druga wersja wszerz(D, L), bez uzycia reverse/2 i append/3
% jest to przechodzenie poziomami, natomiast nie ma gwarancji ze kolejnosc elementow bedzie od lewej do prawej; a nawet na pewno wiadomo ze tak nie bedzie dla wiekszych drzew
wszerzBST2(D, L) :-
    wszerz2([D], [], L).
wszerz2([], [], []).
wszerz2([], NastepnyPoziom, A) :-
    wszerz2(NastepnyPoziom, [], A).
wszerz2([nil|Kolejka], NastepnyPoziom, A) :-
    wszerz2(Kolejka, NastepnyPoziom, A).
wszerz2([t(L,X,R)|Kolejka], NastepnyPoziom, [X|A]) :-
    wszerz2(Kolejka, [L,R|NastepnyPoziom], A).

% ======= Struktury otwarte i różnicowe ========

%% 1. Implementacja kolejki FIFO, czyli:
%% a) init(Kolejka) - inicjalizacja kolejki (na pustą)
%% b) get(Elem, Kolejka, NowaKolejka) - pobranie
%% c) put(Elem, Kolejka, NowaKolejka) - wstawienie
%% d) empty(Kolejka) - czy kolejka pusta
get2(Elem, [Elem], []).
get2(Elem, [H|T], [H|NT]) :-
    get2(Elem, T, NT).
put2(Elem, Kolejka, [Elem|Kolejka]).
init2([]).
empty2([]).
/* Nie mam wielkiego przekonania ze to o cos takiego chodzilo, ale jest to implementacja kolejki, zrobiona na zwyklej liscie. Nie widzialem potrzeby wprowadzania jakiegos termu oznaczajacego kolejke.

?- init(Q), put(1, Q, Q1), put(2, Q1, Q2), put(3, Q2, Q3), put(4, Q3, Q4), get(E, Q4, Q4_1).
Q = [],
Q1 = [1],
Q2 = [2, 1],
Q3 = [3, 2, 1],
Q4 = [4, 3, 2, 1],
E = 1,
Q4_1 = [4, 3, 2] ;
false.
*/

% q(PutElementsHere, GetElementsFromHere)
init(q([], [])).
empty(q([], _)).
put(E, q(Q, G), q([E|Q], G)).
get(RH, q(Q, []), q([], RT)) :-
    reverse(Q, [RH|RT]).
get(GH, q(Q, [GH|GT]), q(Q, GT)).
/* Jednak chyba znalazlem powod dla tego cwiczenia (optymalizacja, zeby nie przechodzic/odwracac listy gdy nie ma takiej potrzeby, czyli np. jesli dodalismy 1000 elementow i teraz je pobieramy, to reverse powinien miec miejsce tylko raz, a nie przy kazdym pobraniu. Dodatkowo, mozna w tym czasie w stalym czasie dodawac kolejne elementy, reverse wykona sie dopiero kiedy nie bedzie mozna odpowiedziec na pytanie jaki teraz element jest na poczatku kolejki)

?- init(Q), put(1, Q, Q11), put(2, Q11, Q12), put(3, Q12, Q13), put(4, Q13, Q14), get(E1, Q14, Q24), put(5, Q24, Q25), put(6, Q25, Q26), get(E2, Q26, Q36), get(E3, Q36, Q46), get(E4, Q46, Q56), get(E5, Q56, Q66), get(E6, Q66, Qempty), empty(Qempty).
Q = Qempty, Qempty = q([], []),
Q11 = q([1], []),
Q12 = q([2, 1], []),
Q13 = q([3, 2, 1], []),
Q14 = q([4, 3, 2, 1], []),
E1 = 1,
Q24 = q([], [2, 3, 4]),
Q25 = q([5], [2, 3, 4]),
Q26 = q([6, 5], [2, 3, 4]),
E2 = 2,
Q36 = q([6, 5], [3, 4]),
E3 = 3,
Q46 = q([6, 5], [4]),
E4 = 4,
Q56 = q([6, 5], []),
E5 = 5,
Q66 = q([], [6]),
E6 = 6 ;
false.
*/


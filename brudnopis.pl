%% Lepsze wypisywanie opisów dla konfliktów

process(_, _, [Stan|_], _, _, null, konflikt(Opis)) :-
    Stan = stan(_, _, shift_reduce), !,
    conflictDesc(Stan, Opis).
process(_, _, [Stan|_], _, _, null, konflikt(Opis)) :-
    Stan = stan(_, _, reduce_reduce), !,
    conflictDesc(Stan, Opis).
conflictDesc(stan(_, _, shift_reduce), 'Shift/Reduce').
conflictDesc(stan(_, _, reduce_reduce), 'Reduce/Reduce').


/* showcase jak analizuję gramatykę i tworzę listę reguł w ramach closure

│ GRAMATYKA - symbol poczatkowy: A
│
│ A -> xB | By | ε
│ B -> ε


│ REGUŁY
│
│ (ID, 0): A -> xB
│ (ID, 0): A -> By
│ (ID, 0): A -> ε
│ (ID, 0): B -> ε

Rozwiniete nieterminale to []

│ REGUŁY
│
│ (ID, 0): A -> xB

G = gramatyka('A', [prod('A', [[x, nt('B')], [nt('B'), y], []]), prod('B', [[]])]),
L = [regula('A', 0, [x, nt('B')]), regula('A', 0, [nt('B'), y]), regula('A', 0, []), regula('B', 0, [])],
H = regula('A', 0, [x, nt('B')]),
T = [regula('A', 0, [nt('B'), y]), regula('A', 0, []), regula('B', 0, [])],
R = [regula('A', 0, [x, nt('B')])] ;

*/

/*
│ GRAMATYKA - symbol poczatkowy: A
│
│ A →  xA | x


│ REGUŁY
│
│ (0) Z →  .A#
│ (0) A →  .xA
│ (0) A →  .x

Rozwiniete nieterminale to [A]

│ REGUŁY
│
│ (0) A →  .x
│ (0) A →  .xA
│ (0) Z →  .A#

G = gramatyka('A', [prod('A', [[x, nt('A')], [x]])]),
L = [regula('Z', 0, [nt('A'), #]), regula('A', 0, [x, nt('A')]), regula('A', 0, [x])],
H = regula('Z', 0, [nt('A'), #]),
T = [regula('A', 0, [x, nt('A')]), regula('A', 0, [x])],
R = [regula('A', 0, [x]), regula('A', 0, [x, nt('A')]), regula('Z', 0, [nt('A'), #])] ;

*/


/*create_state(Id, RegulyStartowe, Stan) :-
    AutoKreator = (WszystkieReguly, WszystkieStany),
    closure(WszystkieReguly, RegulyStartowe, RozwinietaListaRegul),
    member(state(_, RozwinietaListaRegul), WszystkieStany),
    Stan = state()*/

%% createLR(+Gramatyka, -Automat, -Info)

/*createLR(Gramatyka, Automat, Info) :-
    rules_list(Gramatyka, [R|T]),
    closure([R|T], [R], RozwiniecieStartowejReguly),
    KolejkaStanow = [state(0, RozwiniecieStartowejReguly)],
    createAuto(WszystkieReguly, KolejkaStanow, KolejkaStanow, [], WszystkieAkcje).

createAuto(_, [], _, A, A).

createAuto(Reguly, [S|T], Stany, Akumulator, Akcje) :-
    S = state(Id, RegulyStanu),
    processState(S, Reguly, Stany, WiecejStanow)
    createAuto(Reguly, T, _WiecejStanow, _WiecejAkcji, Akcje).
*/
/*
process_state(S, Reguly, NoweStany) :-
    S = stan(_, RegulyStanu),
    process_state_rules(RegulyStanu, Reguly, */


